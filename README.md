# Tools

Tools to make your day to day life easier as a developer.

| name               | description |
|--------------------|-------------|
| clip               | Abstract use of xclip in a nice way to copy stuff from STDIN or file(s). |
| regalia            | A simple package manager to install the latest release of third party packages that can be useful in our day to day lives. |
| set-git-aliases    | Add some extended "commands" to git, which can prove useful in your workflow. |
| burnote            | Send temporary notes over gcp, like a shared, secret paste buffer. |
| tq                 | An easy way to (mainly) query SQL servers/databases -- "Tsql Query" |

All tools should have a `--help` flag for more information on how to use them.

To install them on the system, run `make install`, or `make help` for other options.
