#!/bin/bash

get_help() {
	cat <<- EOF
		Usage: $(basename "$0") [-p|--paste] FILE...

		Copy FILE(s) content to clipboard or paste from clipboard.

		Options:
		  -p, --paste    Paste content from clipboard.
		  -h, --help     Show this help message and exit.
	EOF
	exit 0
}

paste=false

# Parse options using getopt
PARSED_OPTIONS=$(getopt -o ph --long paste,help -- "$@") || exit 1

eval set -- "$PARSED_OPTIONS"

while true; do
	case "$1" in
		-p | --paste)
			paste=true
			shift
			;;
		-h | --help)
			get_help
			;;
		--)
			shift
			break
			;;
		*)
			echo "Invalid option: $1"
			exit 1
			;;
	esac
done

if [[ $paste == true ]]; then
	temp_file=$(mktemp -p /tmp "$(basename "$0").XXXXX")
	trap 'rm -rf "$temp_file"' EXIT

	xclip -selection clipboard -out > "$temp_file"
	awk 1 "$temp_file"

elif (($# == 0)) && [[ -t 0 ]]; then
	echo "No files provided. Exiting."
	exit 1

else
	for file in "$@"; do
		[[ -e $file ]] || {
			echo "$file: Not a file, can't add to clipboard. Exiting." >&2
			exit 2
		}
	done

	(($#)) || set -- /dev/stdin
	sed 's/\r$//' "${@}" | xclip -selection clipboard
fi
