#!/bin/bash
help() {
	figlet 1>&2 "Regalia"
	cat <<- EOF
		Usage: sudo $script_name [OPTIONS] COMMAND [ARGS]...

		Description:
		  Regalia - Noun
		  Equipment or apparatus that is used for a particular purpose.

		Options:
		  -d, --dest-dir DIR  Set the destination directory for installations (default: /usr/local/bin)
		  -f, --force         Will over-write install even if it reports same version as remote source.
		  -h, --help          Show this message and exit.

		Commands:
		  install   Install the specified package or packages.
		  remove    Remove the specified package or packages.
		  upgrade   Upgrade all installed packages to latest version.
		  status    Get installation information on all installable packages.
		  help      Display this help message.

		Available packages:
		  ${available_tools//$'\n'/$'\n'  }

		Examples:
		  sudo $script_name install kustomize   Install the "kustomize" package.
		  sudo $script_name remove kustomize    Remove the "kustomize" package.
		  sudo $script_name install all         Install all available packages.
		  sudo $script_name help                Display this help message.
	EOF
	exit 0
}

set -o pipefail
DESTDIR=${DESTDIR:-/usr/local/bin}
NOT_INSTALLED="not installed"
IDENTICAL_VERSION=199

script_name=$(basename "${BASH_SOURCE[0]}")
temp_files_list=$(mktemp --tmpdir=/tmp temp_files_list.XXXX)

cleanup_temp_files() {
	readarray -d '' temp_files < "$temp_files_list"
	rm --force "${temp_files[@]}" "$temp_files_list"
}

trap 'cleanup_temp_files' EXIT

####################
# Helper functions #
####################

err() {
	local last_exit_code=$?
	local bold_red='\033[1;31m' norm='\033[0m'

	printf "$bold_red%s:$norm %s\n" "ERROR" "$1" 1>&2

	((last_exit_code == 0)) && last_exit_code=1
	exit $last_exit_code
}

dep() { ## Looks if a package is installed, not the command itself. Small difference, but noticable one.
	local missing pkg

	for pkg in "$@"; do
		dpkg --status "$pkg" &> /dev/null || missing+=("$pkg")
	done

	[[ -n ${missing[*]} ]] && err "Missing dependencies: ${missing[*]}"$'\n'"Install and try again."
}

info() {
	local message=$1 banner green='\033[32m' norm='\033[0m'

	[[ -n $1 ]] || message=$(sed '/^$/d' /dev/stdin)

	if [[ -n $message ]]; then
		banner="====${message//?/=}"
		printf "$banner\n  $green%s$norm\n$banner\n\n" "$message"
	fi
}

temp() {
	local temp_file suffix=$1
	temp_file=$(mktemp --tmpdir=/tmp "$name.XXXX.$suffix")

	printf "%s\0" "$temp_file" >> "$temp_files_list"
	echo "$temp_file"
}

extract_semver() {
	local ver="${1:-"$(< /dev/stdin)"}"
	grep --perl-regexp --only-matching 'v?\K[0-9]+?(\.[0-9]+){1,2}' <<< "$ver" | head --lines 1
}

check_version() {
	remote_version=$(extract_semver "$remote_version")
	current_version=$(
		{
			"$DESTDIR/$name" --version \
				|| "$DESTDIR/$name" version \
				|| "$DESTDIR/$name" -v \
				|| "/usr/bin/$name" --version
		} 2> /dev/null | extract_semver \
			|| echo "$NOT_INSTALLED"
	)
	[[ $current_version == "$remote_version" ]] && return $IDENTICAL_VERSION || return 0
}

curl_progress() {
	local url=$1 target=$2

	((COLUMNS > 107)) && export COLUMNS=107

	curl --silent --head --location --write-out '%{url_effective}' "$url" \
		| awk 'END{print}' \
		| xargs curl --progress-bar --output "$target"
}

is_debian_testing() {
	local testing host_codename
	testing=$(curl -s https://www.debian.org/releases/testing/ | grep -Pom1 '.*Debian &ldquo;\K[[:alnum:]]+')
	host_codename=$(grep -Po 'VERSION_CODENAME=\K\S+' /etc/os-release)
	[[ $testing == "$host_codename" ]]
}

##################################
# Installation settings - Custom #
##################################

terraform_settings() {
	local temp_zip asset_url

	dep unzip curl

	temp_zip=$(temp zip)
	asset_url=$(curl --silent --location "https://www.$name.io/downloads" \
		| sed 's/http/\n&/g' \
		| grep --perl-regexp --only-matching --max-count=1 '.*\Khttps.*linux_amd64\.zip')

	download_cmd=(curl_progress "$asset_url" "$temp_zip")
	post_download_cmd=(unzip -oq "$temp_zip" "$name" -d "$DESTDIR")
	remove_cmd=(rm "$DESTDIR/$name")
	remote_version=$(cut --delimiter / --fields 5 <<< "$asset_url")
}

##################################
# Installation settings - GitHub #
##################################

github_latest() { ## Gets asset_url and remote version
	local url=https://api.github.com/repos/$1/releases/latest select=$2 json

	dep jq curl

	json=$(curl --silent --location "$url")
	[[ $json =~ "API rate limit exceeded for "\S+ ]] && err "${BASH_REMATCH[0]} Try again later."

	asset_url=$(jq -r ".assets[].browser_download_url | select($select)" <<< "$json")
	remote_version=$(jq -r '.tag_name' <<< "$json" | extract_semver)
	[[ -z $remote_version ]] && remote_version=$(jq -r .body <<< "$json" | extract_semver)
}

hadolint_settings() {
	github_latest "$name/$name" 'endswith("Linux-x86_64")'
	download_cmd=(curl_progress "$asset_url" "$DESTDIR/$name")
	post_download_cmd=(chmod +x "$DESTDIR/$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

zola_settings() {
	local codename temp_deb

	if is_debian_testing; then
		codename=testing
	else
		codename=$(grep -Po 'VERSION_CODENAME=\K\S+' /etc/os-release)
	fi

	temp_deb=$(temp deb)

	github_latest "barnumbirr/zola-debian" 'endswith("'"$codename"'.deb")'
	download_cmd=(curl_progress "$asset_url" "$temp_deb")
	post_download_cmd=(apt install --fix-missing "$temp_deb")
	remove_cmd=(apt purge "$name" --yes)
}

csv2xlsx_settings() {
	local temp_tar
	temp_tar=$(temp tar.gz)

	github_latest "mentax/$name" 'endswith("Linux_x86_64.tar.gz")'
	download_cmd=(curl_progress "$asset_url" "$temp_tar")
	post_download_cmd=(tar --extract --file "$temp_tar" --directory "$DESTDIR" "$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

hcl2json_settings() {
	github_latest "tmccombs/$name" 'endswith("linux_amd64")'
	download_cmd=(curl_progress "$asset_url" "$DESTDIR/$name")
	post_download_cmd=(chmod +x "$DESTDIR/$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

kind_settings() {
	github_latest "kubernetes-sigs/$name" 'endswith("linux-amd64")'
	download_cmd=(curl_progress "$asset_url" "$DESTDIR/$name")
	post_download_cmd=(chmod +x "$DESTDIR/$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

kustomize_settings() {
	local temp_tar
	temp_tar=$(temp tar.gz)

	github_latest "kubernetes-sigs/$name" 'endswith("linux_amd64.tar.gz")'
	download_cmd=(curl_progress "$asset_url" "$temp_tar")
	post_download_cmd=(tar --extract --file "$temp_tar" --directory "$DESTDIR" "$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

kubeval_settings() {
	local temp_tar
	temp_tar=$(temp tar.gz)

	github_latest "instrumenta/$name" 'endswith("linux-amd64.tar.gz")'
	download_cmd=(curl_progress "$asset_url" "$temp_tar")
	post_download_cmd=(tar --extract --file "$temp_tar" --directory "$DESTDIR" "$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

nvim_settings() {
	local temp_tar
	temp_tar=$(temp tar.gz)

	github_latest "neovim/neovim" 'endswith("linux64.tar.gz")'
	download_cmd=(curl_progress "$asset_url" "$temp_tar")
	post_download_cmd=(bash -c "tar --extract --file '$temp_tar' --overwrite --directory /usr/local/share \
		&& ln --symbolic --force '/usr/local/share/nvim-linux64/bin/$name' '$DESTDIR/$name' \
		&& mkdir --parents /usr/local/share/man/man1 \
		&& ln --symbolic --force /usr/local/share/nvim-linux64/man/man1/nvim.1 /usr/local/share/man/man1/nvim.1")
	remove_cmd=(rm --recursive "$DESTDIR/$name" "/usr/local/share/nvim-linux64" "/usr/local/share/man/man1/nvim.1")
}

oc_settings() {
	local temp_tar
	temp_tar=$(temp tar.gz)

	github_latest "openshift/origin" 'endswith("linux-64bit.tar.gz") and contains("client")'
	download_cmd=(curl_progress "$asset_url" "$temp_tar")
	post_download_cmd=(tar --extract --file "$temp_tar" --warning no-unknown-keyword --strip-components 1 --directory "$DESTDIR" --wildcards "openshift-*/$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

shfmt_settings() {
	github_latest "mvdan/sh" 'endswith("linux_amd64")'
	download_cmd=(curl_progress "$asset_url" "$DESTDIR/$name")
	post_download_cmd=(chmod +x "$DESTDIR/$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

vale_settings() {
	local temp_tar
	temp_tar=$(temp tar.gz)

	github_latest "errata-ai/$name" 'contains("Linux_64-bit")'
	download_cmd=(curl_progress "$asset_url" "$temp_tar")
	post_download_cmd=(tar --gunzip --extract --file "$temp_tar" --directory "$DESTDIR" "$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

wrench_settings() {
	local temp_tar
	temp_tar=$(temp tar.gz)

	github_latest "cloudspannerecosystem/$name" 'contains("linux-amd64")'
	download_cmd=(curl_progress "$asset_url" "$temp_tar")
	post_download_cmd=(tar --gunzip --extract --file "$temp_tar" --directory "$DESTDIR" "$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

yj_settings() {
	github_latest "sclevine/yj" 'endswith("linux-amd64")'
	download_cmd=(curl_progress "$asset_url" "$DESTDIR/$name")
	post_download_cmd=(chmod +x "$DESTDIR/$name")
	remove_cmd=(rm "$DESTDIR/$name")
}

############
# The deed #
############

install_tool() {
	local name=$1

	info "Downloading and installing..."
	check_version || {
		if (($? == IDENTICAL_VERSION)) && ((force != 1)); then
			info "Remote source reporting identical version; you are up to date!"
			return
		fi
	}

	if [[ ${current_version} == "$NOT_INSTALLED" ]]; then
		unset old_version
	else
		old_version="$current_version"
		info "Currently installed version: $current_version"
	fi

	"${download_cmd[@]}" && "${post_download_cmd[@]}" || return $?

	printf '\n'

	if check_version || (($? == IDENTICAL_VERSION)); then
		sed "s/\S\+/\n${old_version+Now }installed version: &/; s/\S/\u&/" <<< "$current_version" \
			| info
	fi
}

remove_tool() {
	local name=$1

	info "Removing install..."

	"${remove_cmd[@]}" || return $?

	info "Removal of $name succeeded."
}

package_status() {
	local spacer=16 norm='\033[0m' bold='\033[1m' green='\033[32m' yellow='\033[33m' red='\033[31m' color status_icon

	info "Checking status of tools"
	printf "$bold  %-${spacer}s %-${spacer}s %s$norm\n" "Name" "Local" "Latest"
	for name in "${tools_to_install[@]}"; do

		"${name}_settings"
		check_version

		if (("$?" == "$IDENTICAL_VERSION")); then
			color="$green"
			status_icon='\xE2\x9C\x94' # v check!

		elif [[ $current_version == "$NOT_INSTALLED" ]]; then
			color="$yellow"
			status_icon='\xe2\x80\x95' # - N/A

		else
			color="$red"
			status_icon='\xe2\x9c\x97' # x not up to date
		fi

		printf "$color%b %-${spacer}s$norm %-${spacer}s %s\n" "$status_icon" "$name" "$current_version" "$remote_version"
	done

	exit 0
}

set_upgrade_packages() {
	local num_names=${#tools_to_install[*]} name

	for ((i = 0; i < num_names; i++)); do
		name="${tools_to_install[$i]}"
		"${name}_settings"

		check_version
		return_value=$?
		if ((return_value == IDENTICAL_VERSION)) || [[ $current_version == "$NOT_INSTALLED" ]]; then
			unset "tools_to_install[$i]"
		fi
	done

	if ((${#tools_to_install[*]} == 0)); then
		info "All packages are up to date!"
		exit 0
	fi
}

dep figlet
unset install remove upgrade pkg_status force tools_to_install
available_tools=$(sed --quiet --regexp-extended 's/^(\S+)_settings().*/\1/p' "${BASH_SOURCE[0]}")
test -z "$1" && help

until test -z "$1"; do
	case "$1" in
		help | --help | -h) help ;;
		install | --install) install=1 ;;
		remove | --remove) remove=1 ;;
		upgrade | --upgrade)
			upgrade=1
			install=1
			;;
		status | --status) pkg_status=1 ;;
		-f | --force) force=1 ;;
		-d | --dest-dir)
			DESTDIR="$2"
			shift
			;;
		--)
			shift
			break
			;;
		all) readarray -t tools_to_install <<< "$available_tools" ;;
		*) tools_to_install+=("$1") ;;
	esac
	shift
done

tools_to_install+=("$@") # if "--" is used for whatever reason

for name in "${tools_to_install[@]}"; do
	grep --quiet "^$name$" <<< "$available_tools" \
		|| err "$name is not a valid command/package option. Use 'help' for more information."
done

if (($pkg_status$upgrade)); then
	((${#tools_to_install[*]} == 0)) && readarray -t tools_to_install <<< "$available_tools"

	if ((pkg_status)); then
		package_status

	elif ((upgrade)); then
		set_upgrade_packages
	fi
fi

(($(id --user) != 0)) && err "Run this with sudo/as root!"
[[ -z ${tools_to_install[*]} ]] && err "No tools specified for installation or removal."

for name in "${tools_to_install[@]}"; do
	"${name}_settings"
	figlet "$name"
	printf '\n'

	if ((install)); then
		install_tool "$name" || err "Installation failed for $name..."

	elif ((remove)); then
		remove_tool "$name" || err "Removal failed for $name..."

	else
		err "No command given. Use 'help' for more information."
	fi
done
