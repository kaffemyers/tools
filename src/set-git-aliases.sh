#!/bin/env bash
# shellcheck disable=2001,2034,2086,2155

read -rd '' help_section <<- EOF
	Usage: $0 [options]
	  Options:
	    -h, --help        Display this help message
	    -r, --remove      Remove existing aliases first

	  Description:
	    All functions in this script will be added as git aliases by the same name
	    as the function. It's easier for a bash scripter such as me. ¯\_(ツ)_/¯
	    Make note that all functions/aliases should be /bin/sh compliant
EOF

alias() {
	local description="Show list of available aliases. Can take flag -v/--verbose."

	git config --get-regexp '^alias\.' \
		| case "$1" in
			-v | --verbose)
				sed "s/^alias\.\(\S\+\).*/\n\1/; /^}$/{n;d}" \
					| sed "1d"
				;;
			*)
				awk -F\" '/^alias\./{
					sub(/alias\./,"")
					sub(/ .*/,"")
					a=$1
					while($0 !~ /local *description/) getline
					printf "%-15s %s\n", a, $2
				}'
				;;
		esac
}

default_branch() {
	local description="Get the default branch of git repo"
	git remote show origin | grep -Po '\s+HEAD branch: \K.*'
}

search() {
	local description="Search files in repository for ARG1 (case insensitive)"
	git ls-tree -r -z --name-only HEAD \
		| xargs -0 grep --with-filename --ignore-case --line-number --color "$1"
}

search_replace() {
	local description="Replace ARG1 with ARG2 in all tracked files (case sensitive/awk regex)"
	local workfile=/tmp/search_replace_$RANDOM$RANDOM$RANDOM \
		changed_files=/tmp/change_files_$RANDOM$RANDOM$RANDOM
	touch $changed_files

	git ls-tree -r --name-only HEAD | while IFS= read -r file; do
		awk -v from="$1" -v to="$2" '{ gsub(from,to); print }' "$file" > $workfile
		diff $workfile "$file" || echo "$file" >> $changed_files
		cat $workfile > "$file"
	done

	[ -z "$(cat $changed_files)" ] || {
		echo "Files with changes:"
		cat $changed_files
	}
	rm $workfile $changed_files
}

rebranch() {
	local description="Can be used after a merge to recreate the branch with an updated master"
	local current_branch=$(git branch --show-current)
	local \
		default_branch=$(git default-branch) \
		new_branch=${1:-$current_branch} \
		stashed=0

	echo "Current branch: ${current_branch}"
	[ "${current_branch}" != "${default_branch}" ] || {
		echo "On default branch ${default_branch}, nothing to do."
		return
	}
	git stash | grep --quiet "No local changes to save" || stashed=1
	git checkout "${default_branch}"
	git branch -D "${new_branch}"
	git reset --hard "origin/${default_branch}"
	git pull
	git checkout -b "${new_branch}"
	[ $stashed -eq 0 ] || git stash pop
}

purge() {
	local description="Destroy all local branches except default branch"
	local \
		current_branch=$(git branch --show-current) \
		default_branch=$(git default-branch) \
		branches \
		choice

	branches=$(git branch | awk -v d=" $default_branch$" '$0!~d{ print $NF }')
	if [ -n "$branches" ]; then
		[ "${current_branch}" != "${default_branch}" ] && git checkout "${default_branch}"
		git pull
		echo "Branches to be removed"
		echo "$branches" | sed 's/^/    /'
		read -rp "Continue? " choice
		choice=$(echo "$choice" | tr '[:upper:]' '[:lower:]')
		case "$choice" in
			y | yes | yep) git branch -D $branches ;;
			n | no | nope) echo "OK, exiting out..." ;;
			*)
				printf '\n%s\n' 'Answer yes or no.'
				echo
				git purge
				;;
		esac
	else
		echo "No local branches to remove! :)"
	fi
}

bump() {
	local description="Bump the version of the project"
	local \
		default_branch=$(git default-branch) \
		bump_type=${1:-patch} \
		current_version \
		new_version \
		sed_match \
		file

	cd "$(git rev-parse --show-toplevel)" || return 1

	if [ "$bump_type" != patch ] && [ "$bump_type" != minor ] && [ "$bump_type" != major ]; then
		echo "ERROR: bump type supplied incorrect. Must be either major, minor or patch [default: patch]. Supplied: $bump_type" 1>&2
		return 1

	else
		for file in Cargo.toml VERSION pyproject.toml; do
			local toml_field="" temp

			if [ -f "$file" ]; then
				case $file in
					Cargo.toml)
						toml_field=.package.version
						current_version=$(git show $default_branch:$file | tomlq -r $toml_field)
						;;
					VERSION)
						current_version=$(git show $default_branch:$file)
						;;
					pyproject.toml)
						toml_field=.tool.poetry.version
						current_version=$(git show $default_branch:$file | tomlq -r $toml_field)
						;;
				esac

				IFS=. read -r major minor patch <<- EOF
					$current_version
				EOF

				case $bump_type in
					major)
						major=$((major + 1))
						minor=0
						patch=0
						;;
					minor)
						minor=$((minor + 1))
						patch=0
						;;
					patch) patch=$((patch + 1)) ;;
				esac

				new_version="$major.$minor.$patch"
				sed_match=$(echo "$current_version" | sed 's/\./\\./g')

				if [ -n "$toml_field" ]; then

					temp=$(tomlq --arg n "$new_version" -t "$toml_field = \$n" "$file")
					[ -z "$temp" ] && return 1
					echo "$temp" > "$file"

				else
					sed -i "0,/$sed_match/{s/$sed_match/$new_version/}" "$file"
				fi

				printf 'Bumped the version in %s: %s -> %s\n' "$file" "$current_version" "$new_version"
			fi
		done
	fi

	[ -n "$current_version" ] || echo "Nothing to bump!"
}

case "$1" in
	-h | --help)
		echo "$help_section"
		exit
		;;
	-r | --remove)
		git config --get-regexp '^alias\.' \
			| grep -Eo '^alias\.\S+' \
			| xargs -I{} git config --global --unset {}
		;;
esac

while IFS= read -r line; do
	function_array+=("$line")

	if [[ $line == "}" ]]; then
		name="${function_array[0]/ */}"
		name="${name//-/_}"
		printf -v alias_string '%s\n' "${function_array[@]}"

		git config --global alias."${name//_/-}" "!$alias_string$name"

		unset function_array
	fi
done < <(typeset -f)
