#!/usr/bin/env python3

import csv
import json
import logging
import sys
from datetime import datetime, timezone
from decimal import Decimal
from pathlib import Path

import click
import pyodbc
import sqlparse
import yaml
from pyodbc import Connection, Cursor, Row
from sqlparse.exceptions import SQLParseError
from tabulate import tabulate

# Configuration example for database connection
config_example = """
    Driver={ODBC Driver 18 for SQL Server}
    Server=tcp:wsp4001c.sebank.se
    Port=1443
    Database=InstrumentStatic
    UID=MY_COOL_USER
    PWD=MY_SECRET_PASSWORD
    TrustServerCertificate=yes
"""

logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")


# Classes for custom exceptions
class InvalidSQLQueryError(ValueError):
    def __init__(self):
        super().__init__("Invalid SQL query")


class InvalidTestSQLQueryError(ValueError):
    def __init__(self):
        super().__init__("Invalid SQL query: 'test' is not a valid SQL command")


def write(string: str) -> None:
    sys.stdout.write(f"{string}\n")


def int_chop(bts: bytes, start: int, end: int) -> int:
    return int.from_bytes(bts[start:end], byteorder="little")


def handle_datetime_with_timezone_type(bts: bytes) -> str:
    year = int_chop(bts, 0, 2)
    month = int_chop(bts, 2, 4)
    day = int_chop(bts, 4, 6)
    hour = int_chop(bts, 6, 8)
    minute = int_chop(bts, 8, 10)
    second = int_chop(bts, 10, 12)
    microsecond = int_chop(bts, 12, 16)
    offset_hours = int_chop(bts, 16, 18)
    offset_minutes = int_chop(bts, 18, 20)

    dt = datetime(year, month, day, hour, minute, second, microsecond, tzinfo=timezone.utc)

    formatted_dt = dt.strftime("%Y-%m-%d %H:%M:%S.%f")
    offset = f"+{offset_hours:02}:{offset_minutes:02}"

    return f"{formatted_dt} {offset}"


def load_connection_config(config_file: str) -> str:
    try:
        connection_config = Path(config_file).read_text().splitlines()
        connection_config = [line for line in connection_config if not line.split()[0].startswith("#") if line]
        return ";".join(connection_config)
    except FileNotFoundError:
        logging.exception(f"'{config_file}' does not exist, crapping out...")
        sys.exit(1)


def connect_to_database(connection_string: str) -> tuple[Connection, Cursor]:
    try:
        connection = pyodbc.connect(connection_string, read_only=True)
        connection.add_output_converter(-155, handle_datetime_with_timezone_type)
        cursor = connection.cursor()
    except pyodbc.InterfaceError:
        logging.exception("Config file incorrectly formatted. Config example:")
        logging.exception("\n".join([line.strip() for line in config_example.splitlines()]))
        sys.exit(1)
    except pyodbc.Error:
        logging.exception("Error connecting to the database.")
        sys.exit(1)
    else:
        return connection, cursor


def _lint_sql(query: str) -> str:
    formatted_query = sqlparse.format(query, reindent=True, keyword_case="upper")
    parsed = sqlparse.parse(formatted_query)
    if not parsed:
        raise InvalidSQLQueryError
    return formatted_query or ""


def lint_sql(query: str) -> str:
    try:
        formatted_query = _lint_sql(query)
    except (SQLParseError, InvalidSQLQueryError):
        logging.exception("SQL linting failed")
        sys.exit(1)
    else:
        return formatted_query


def validate_sql(query: str) -> None:
    if query.strip().lower().startswith("test"):
        raise InvalidTestSQLQueryError


def execute_query(cursor: Cursor, query: str) -> tuple[list[str], list[Row]]:
    try:
        query = lint_sql(query)
        validate_sql(query)
        cursor.execute(query)
        headers = [i[0] for i in cursor.description]
        rows = cursor.fetchall()
    except pyodbc.Error:
        logging.exception("Could not execute query!")
        logging.exception(query)
        sys.exit(1)
    except ValueError:
        logging.exception("ValueError")
        sys.exit(1)
    else:
        return headers, rows


def convert_if_decimal(value: Decimal | str) -> str:
    return str(value) if isinstance(value, Decimal) else value


def format_and_output_data(output: str, headers: list[str], rows: list[Row]) -> None:
    if output == "json":
        result = [dict(zip(headers, row)) for row in rows]
        write(json.dumps(result, default=str, indent=2))

    elif output == "csv":
        csv_writer = csv.writer(sys.stdout)
        csv_writer.writerow(headers)
        csv_writer.writerows(rows)

    elif output == "yaml":
        result = [dict(zip(headers, [convert_if_decimal(value) for value in row])) for row in rows]
        write(yaml.safe_dump(result))

    else:
        table = tabulate(rows, headers, tablefmt="grid")
        write(table)


@click.command(help="Query a SQL database with a given query from STDIN and format the result.")
@click.option(
    "--output",
    "-o",
    type=click.Choice(["json", "csv", "yaml", "table"]),
    default="table",
    help="Output format [default: table]",
)
@click.option("--config", "-c", type=click.Path(exists=True), help="Config file path", required=True)
@click.option("--list-tables", "-l", is_flag=True, help="List all tables")
@click.option("--list-procedures", "-p", is_flag=True, help="List all stored procedures")
@click.option("--show-procedure", "-s", help="Show the definition of a stored procedure")
def main(*, output: str, config: str, list_tables: bool, list_procedures: bool, show_procedure: str | None) -> None:
    connection_string = load_connection_config(config)
    connection, cursor = connect_to_database(connection_string)

    if list_tables:
        query = """
            SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
        """
    elif list_procedures:
        query = """
            SELECT ROUTINE_NAME FROM INFORMATION_SCHEMA.ROUTINES
            WHERE ROUTINE_TYPE='PROCEDURE'
        """
    elif show_procedure:
        query = """
            SELECT ROUTINE_DEFINITION FROM INFORMATION_SCHEMA.ROUTINES
            WHERE ROUTINE_TYPE='PROCEDURE' AND ROUTINE_NAME = ?
        """
    else:
        query = click.get_text_stream("stdin").read()

    if show_procedure:
        cursor.execute(query, (show_procedure,))
        fetched = cursor.fetchone()
        procedure_definition = fetched[0] if fetched is not None else ""
        write(procedure_definition)
    else:
        headers, rows = execute_query(cursor, query)
        format_and_output_data(output, headers, rows)

    cursor.close()
    connection.close()


if __name__ == "__main__":
    main()
