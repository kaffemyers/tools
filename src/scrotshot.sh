#!/usr/bin/env bash
SCRIPT_NAME=$(basename "$0")

[[ -z $1 ]] && set -- --help

print_help() {
	cat <<- EOF
		Wrapper script for scrot with extra functions, such as upload via scp.
		Set location, name, upload location and screenshot url in scalde settings.

		usage: $SCRIPT_NAME [OPTION]
		  -s, --screen-selection  Prints a selection of the screen, map out with your mouse
		  -d, --desktop           Normal, full desktop print screen
		  -w, --active-window     Screenshots active window
		  -u, --upload            Use scp to upload file. Define upload location in scalde settings file.
		  -r, --remove-original   Remove local copy. Only applies when combined with --upload.
		  -e, --edit              Open up screenshot in editor instead of saving to file
		  -h, --help              Prints sparse help section
	EOF
}

options=(
	--options "sdwureh"
	--longoptions "screen-selection,desktop,active-window,upload,remove-original,edit,help"
	--name "$SCRIPT_NAME"
	-- "$@"
)

OPTS=$(getopt "${options[@]}")
eval set -- "$OPTS"

_notify() {
	if hash notify-send 2> /dev/null; then
		notify-send "$@"
	else
		true
	fi
}

scrotopt="-f"
upload=0
rmorg=0
edit=0

SCROTSHOT_DIR=${SCROTSHOT_DIR:-"$(
	echo "$HOME/screenshots"
	mkdir --parents "$_"
)"}
SCROTSHOT_NAME=${SCROTSHOT_NAME:-'Screenshot_%Y-%m-%d_%H.%M.%S_$wx$h.png'}

while true; do
	case "$1" in
		-s | --screen-selection)
			scrotopt+="s"
			shift
			;;
		-d | --desktop)
			scrotopt+=""
			shift
			;;
		-w | --active-window)
			scrotopt+="u"
			shift
			;;
		-u | --upload)
			upload=1
			shift
			;;
		-r | --remove-original)
			rmorg=1
			shift
			;;
		-e | --edit)
			edit=1
			shift
			;;
		-h | --help)
			print_help
			exit
			;;
		--)
			shift
			break
			;;
		*)
			echo "Internal error!"
			exit 1
			;;
	esac
done

set -euo pipefail

if ! cd "$SCROTSHOT_DIR"; then
	_notify "screenshot directory doesn't exist" \
		"Either create $HOME/screenshots or set preferred directory in scalde settings.\nExample:\nscreenshot-dir = ~/images/screenshots"
	exit 1
fi

if ((edit)); then
	if ! hash drawing 2> /dev/null; then
		_notify "drawing not installed" "Can not open up screenshot for edit"
	fi
	# shellcheck disable=SC2016
	sleep 0.2 && scrot -f $scrotopt -e 'xclip -selection clipboard -t image/png -i $f && drawing -c'
	exit
fi

produced_file=$(sleep 0.2 && scrot -f $scrotopt "$SCROTSHOT_NAME" -e 'echo $f')

if ((upload)); then
	screenshot_url=$(scaldeop -s screenshot-url)
	scp "$produced_file" "$(scaldeop -s screenshot-rdir)"
	xdg-open "$screenshot_url/$produced_file"
	echo "$screenshot_url/$produced_file" | xclip -selection clipboard

	((rmorg)) && rm -v "$produced_file"

	_notify "Screenshot created" "File and url added to clipboard"

else
	_notify "Screenshot created" "$SCROTSHOT_DIR/$produced_file"
fi
