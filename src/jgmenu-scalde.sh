#!/usr/bin/env bash
SCRIPT_NAME=$(basename "$0")

print_help() {
	cat <<- EOF
		Usage: $SCRIPT_NAME [OPTION] {OPTARG}

		Description
		  A wrapper script for scalde styled jgmenu.
		  Creates config and csv file based on arguments and stdin.
		  Each field is a new argument (or new line in stdin).
		  So, to build a csv like "Shown text,Command,Icon", run:
		  $SCRIPT_NAME -f3 "Shown text 1" "Command 1" "Icon 1" \\
		  ${SCRIPT_NAME//?/ }     "Shown text 2" "Command 2" "Icon 3"

		... and so on

		Options:
		  -H, --header <STR>        Set header
		  -f, --fields <INT>        Set number of fields in csv. Default: 2
		  -c, --columns <INT>       Set number of columns to render. Default: 1
		  -t, --text-spacing <INT>  Set spacing between icon and text. Default: set in jgmenurc
		  -n, --no-spawn            Add --no-spawn to jgmenu call
		  -e, --echo-cfg            Echo name of jgmenu config instead of starting an instance
		  -b, --background          Run menu as background process and return pid
		  -h, --help                Prints this help section
	EOF
}

err_exit() {
	printf '%s\n' "Error: $1" 1>&2
	exit "${2:-1}"
}

produce_cfg_file() {
	local menupad sed_script

	menupad=$(sed '/menu_padding_left/!d;s/.*=\s*//;q' "${XDG_CONFIG_HOME:-$HOME/.config}/jgmenu/jgmenurc")

	read -rd '' sed_script <<- EOF
		/^[a-z]/!d
		/^columns/s/=.*/= 1/
		/^menu_width/s/=.*/= 400/
		/^menu_height_min/s/=.*/= 1/
		/^stay_alive/s/=.*/= 0/
		/^menu_padding/s/=.*/= $menupad/
	EOF

	sed "$sed_script" "${XDG_CONFIG_HOME:-$HOME/.config}/jgmenu/jgmenurc"
}

header() {
	local text_color
	text_color=$(awk -F',' '/^@search/{ print $10 "," $11 }' \
		"${XDG_CONFIG_HOME:-$HOME/.config}/jgmenu/prepend.csv")

	echo "@text,,32,10,442,35,2,left,top,$text_color,$1" >> "$csv_file"
	sed -i '/^menu_padding_top/s/=.*/= 40/' "$cfg_file"
}

menu_items() {
	local printf_style
	[[ $1 =~ ^[0-9]+$ ]] || err_exit "--fields needs integer, not $1"
	printf_style=$(eval echo "{1..$1}" | sed 's/\S\+/"""%s"""/g;s/\s/,/g;s/$/\\n/')
	# shellcheck disable=SC2059
	printf "$printf_style" "${@:2}" >> "$csv_file"
}

echo_cfg() {
	cfg_delete=0
	echo "$cfg_file"
}

icon_text_spacing() { sed -i "/^icon_text_spacing/s/=.*/= $1/" "$cfg_file"; }

_clean_up() {
	[[ -f $csv_file ]] && rm -f "$csv_file"
	[[ -f $cfg_file ]] && ((cfg_delete)) && rm -f "$cfg_file"
}

main() {
	# defualt booleans
	local echo_cfg=0 background_pid=0
	cfg_delete=1 # for trapped _clean_up

	# defualt values
	local csv_fields=2 columns=1 pid jgmenu_args=()

	[[ -z $1 ]] && print_help && exit 1

	options=(
		--options "H:f:c:t:nebh"
		--longoptions "header:,fields:,columns:,text-spacing:,no-spawn,echo-cfg,background,help"
		--name "$SCRIPT_NAME"
		-- "$@"
	)

	OPTS=$(getopt "${options[@]}")
	eval set -- "$OPTS"

	csv_file=$(mktemp -p /tmp scalde-jgmenu.XXXXX.csv)
	cfg_file=$(cp -p "$csv_file" "${csv_file%csv}cfg" && echo "$_")

	trap _clean_up EXIT

	jgmenu_args=(--csv-file="$csv_file" --config-file="$cfg_file")
	produce_cfg_file > "$cfg_file"

	while :; do
		case "$1" in
			-H | --header)
				header "$2"
				shift 2
				;;
			-f | --fields)
				csv_fields=$2
				shift 2
				;;
			-c | --columns)
				columns=$2
				shift 2
				;;
			-t | --text-spacing)
				icon_text_spacing "$2"
				shift 2
				;;
			-n | --no-spawn)
				jgmenu_args+=(--no-spawn)
				shift
				;;
			-e | --echo-cfg)
				echo_cfg=0
				shift
				;;
			-b | --background)
				background_pid=0
				shift
				;;
			-h | --help)
				print_help
				exit
				;;
			--)
				shift
				break
				;;
			*) break ;;
		esac
	done

	set -euo pipefail

	((csv_fields < 3)) && sed -i '/^icon_size/s/=.*/= 0/' "$cfg_file"
	((columns > 1)) && sed -i "/^columns/s/=.*/= $columns/" "$cfg_file"

	if [[ -z $* ]]; then
		IFS=$'\n'
		# shellcheck disable=SC2046 # irrelevant because IFS is set accordingly
		set -- $(cat /dev/stdin)
		unset IFS
	fi

	menu_items "$csv_fields" "$@"

	if ((echo_cfg)); then
		echo_cfg

	elif ((background_pid)); then
		jgmenu "${jgmenu_args[@]}" &
		pid=$!
		echo "$pid"
		disown "$pid"
		sleep 0.5 # make sure jgmenu is able to read files before they are removed

	else
		jgmenu "${jgmenu_args[@]}"
	fi
}

main "$@"
