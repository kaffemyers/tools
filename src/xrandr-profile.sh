#!/usr/bin/env bash

SCRIPT_NAME=$(basename "$0")
DEFAULT_PROFILE_DIR="${XDG_CONFIG_HOME:-"$HOME/.config"}/xrandr-profile"
XRANDR_PROFILE_DIR=${XRANDR_PROFILE_DIR:-"$DEFAULT_PROFILE_DIR"}
USAGE="Usage: $SCRIPT_NAME [COMMAND] [OPTIONS]"

print_help() {
	cat <<- EOF
		$USAGE

		Description:
		  $SCRIPT_NAME provides a convenient way to save and load display
		  configurations using xrandr. It captures the current setup and stores
		  it in a YAML file. Later, you can load the stored configuration to
		  restore your display setup.

		Commands:
		  save            Save current display configuration to a YAML file
		  load            Load the stored display configuration from the YAML file
		  exists          Check if a profile exists, exit with code 2 if not
		  print           Print constructed YAML of the current configuration
		                  to stdout

		Options:
		  -q, --quiet     Do every operation silently
		  -d, --dir <DIR> Specify a custom directory for storing/reading YAML files
		                  [env: XRANDR_PROFILE_DIR=$XRANDR_PROFILE_DIR]
		  -h, --help      Show this help message

		Exists option:
		  -m, --match     Exit with code 3 if the parsed, current configuration
		                  does not match the stored command

		Print options:
		  -s, --stored    Use stored YAML instead of constructing a new one
		  -c, --command   Print the xrandr command
		  -i, --id        Print the id
		  -p, --path      Print the path of the profile YAML
	EOF
}

info() {
	local msgs=("$@")
	((quiet)) || printf "%s\n" "${msgs[@]}"
}

err_exit() {
	local msg=$1

	((quiet)) || printf "%s\n" "ERROR: $msg" "For usage information, try: $SCRIPT_NAME --help" 1>&2

	exit 1
}

make_xrandr_command() {
	local xrandr_props=$1 awk_script
	read -rd '' awk_script <<- 'EOF'
		BEGIN { printf "xrandr" }

		/ connected / && NR == FNR {
			output=$1
			gsub(/-/, "_", dp)

			match($0, /[[:digit:]]+x[[:digit:]]+/)
			mode[output] = substr($0, RSTART, RLENGTH)
			split(mode[output], dimensions, "x")
			rev_dimensions = sprintf("%sx%s", dimensions[2], dimensions[1])

			while (getline > 0 && $0 ~ /^[[:space:]]/) {
				if ($1 ~ rev_dimensions) {
					mode[output] = rev_dimensions
				}
			}
		}

		/ connected / && NR != FNR {
			previous_output = output
			output = $1
			printf " --output %s", output

			if($3 == "primary") {
				$3 = ""
				$0 = $0
				printf " --primary"
			}

			if($3 !~ /^[0-9]/) {
				printf " --auto"
				if(previous_output) printf " --right-of %s", previous_output
				next
			}

			split($3, mode_pos, "+")
			pos = mode_pos[2] "x" mode_pos[3]

			sub(/^\(/, "", $4)
			printf " --mode %s --pos %s --rotate %s", mode[output], pos, $4
		}

		/ disconnected / && NR != FNR {
			printf " --output %s --off", $1
		}
	EOF

	awk "$awk_script" <(echo "$xrandr_props") <(echo "$xrandr_props") | jq -R 'split(" ")'
}

collect_monitors_info() {
	local xrandr_props=$1 awk_script

	read -rd '' awk_script <<- 'EOF'
		BEGIN { q = "\"" }

		/ connected / {
			list = ""

			printf "{" q "name" q ":" q "%s" q, $1

			getline
			getline

			indent=$0
			sub(/[^[:blank:]]+/, "", indent)

			printf "," q "edid" q ":["

			while ($0 ~ indent) {
				list = list "," q $1 q
				getline
			}

			printf "%s]}", substr(list, 2)
		}
	EOF

	awk "$awk_script" <<< "$xrandr_props" | jq --slurp
}

make_id() {
	local monitors
	monitors=${1:-"$(collect_monitors_info "$xrandr_props")"}

	jq --raw-output '.[] | .name, .edid[]' <<< "$monitors" | sha256sum | cut -d" " -f1
}

construct_json() {
	local xrandr_props=$1 id monitors command_list

	monitors=$(collect_monitors_info "$xrandr_props")
	id=$(make_id "$monitors")
	command_list=$(make_xrandr_command "$xrandr_props")

	# shellcheck disable=SC2016
	yq -n --yaml-output \
		--argjson monitors "$monitors" \
		--arg id "$id" \
		--argjson command "$command_list" \
		'{
			pretty_id: (
				$monitors
				| map(.name)
				| join(" | ")
			)
			, monitors: $monitors
			, id: $id
			, command: $command
		}'
}

save_yaml() {
	local profile_directory=$1 yaml_content yaml_file id

	yaml_content=$(construct_json "$xrandr_props")
	id=$(yq --raw-output .id <<< "$yaml_content")

	yaml_file="$profile_directory/$id.yaml"
	echo "$yaml_content" > "$yaml_file"

	info "Saved to $yaml_file"
}

load_yaml() {
	local yaml_file="$1/$2.yaml" xrandr_command
	readarray -t xrandr_command < <(yq --raw-output '.command[]' "$yaml_file")

	"${xrandr_command[@]}"
	info "Executed xrandr command from $yaml_file"
}

print_yaml() {
	local id=$2 yaml_file="$1/$2.yaml" yaml_content print_stored=$3 print_command=$4 print_id=$5 print_path=$6

	if ((print_stored)); then
		yaml_content=$(yq --yaml-output . "$yaml_file") || err_exit "Could not parse -- $yaml_file"
	else
		yaml_content=$(construct_json "$xrandr_props")
	fi

	if ((print_path)); then
		info "$yaml_file"
	elif ((print_command)); then
		yq --raw-output '.command | join(" ")' <<< "$yaml_content"
	elif ((print_id)); then
		yq --raw-output '.id' <<< "$yaml_content"
	else
		info "$yaml_content"
	fi
}

exists_check() {
	local yaml_file="$1/$2.yaml" exists_match=$3

	[[ -f $yaml_file ]] || {
		info "No stored configuration found for the current setup in $2"
		exit 2
	}

	if ((exists_match)); then
		stored_command=$(print_yaml 1 1)
		current_command=$(print_yaml 0 1)

		[[ $stored_command == "$current_command" ]] || {
			info "Stored and current configurations differ:" "Stored:  $stored_command" "Current: $current_command"
			exit 3
		}
	fi
}

main() {
	local quiet=0 print_stored=0 print_command=0 id=0 print_path=0 exists_match=0
	local OPTS xrandr_props options=(
		--options "qd:scipmh"
		--longoptions "quiet,dir:,stored,command,id,path,match,help"
		--name "$SCRIPT_NAME" -- "$@"
	)

	OPTS=$(getopt "${options[@]}") || err_exit "Invalid option(s) provided."
	eval set -- "$OPTS"

	while :; do
		case $1 in
			-q | --quiet)
				quiet=1
				shift
				;;
			-d | --dir)
				XRANDR_PROFILE_DIR="$2"
				shift 2
				;;
			-h | --help)
				print_help
				exit 0
				;;
			-s | --stored)
				print_stored=1
				shift
				;;
			-c | --command)
				print_command=1
				shift
				;;
			-i | --id)
				print_id=1
				shift
				;;
			-p | --path)
				print_path=1
				shift
				;;
			-m | --match)
				exists_match=1
				shift
				;;
			--)
				shift
				break
				;;
			*)
				err_exit "Internal error."
				;;
		esac
	done

	config_base_directory=$(dirname "$XRANDR_PROFILE_DIR")

	if [[ -d $config_base_directory ]]; then
		mkdir --parents "$XRANDR_PROFILE_DIR" \
			|| err_exit "Could not create directory -- $XRANDR_PROFILE_DIR"

	else
		err_exit "Configuration base directory does not exist -- $config_base_directory"
	fi

	xrandr_props=$(xrandr --props)

	case $1 in
		save)
			save_yaml "$XRANDR_PROFILE_DIR"
			;;
		load)
			load_yaml "$XRANDR_PROFILE_DIR" "$(make_id)"
			;;
		print)
			print_yaml "$XRANDR_PROFILE_DIR" "$(make_id)" "$print_stored" "$print_command" "$print_id" "$print_path"
			;;
		exist | exists)
			exists_check "$XRANDR_PROFILE_DIR" "$(make_id)" "$exists_match"
			;;
		*)
			if [[ -n $1 ]]; then
				msg="Unknown command -- $1"

			else
				msg="No command given."
			fi

			err_exit "$msg"
			;;
	esac
}

main "$@"
