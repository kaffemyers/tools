#!/bin/bash

set -o pipefail

get_help() {
	cat 2>&1 <<- EOF
		Usage: $script_name <command> [data-file] [options]

		Create or access a pre-defined secret.
		The secret will be deleted automatically after $burn_delay.
		If secret already exists upon creation, it will extend it $burn_delay from now.
		If no data-file is given, it will read from standard input.

		Commands:
		  post [data-file]  Create a "burning note" from the content of data-file (or stdin).
		  read              Print the content of the "burning note" to stdout (redirect to file if needed).

		Options:
		  -s, --secret-id   <id>       Set the secret ID (default: $secret_id)
		  -b, --burn-delay  <delay>    Set the burn delay (default: $burn_delay)
		                               Can use shorthand s, m, h and d.
		                               Defaults to seconds if not specified.
		  -p, --project     <name>     Set GCP project. (default: $project)
		                               Can also be set with env var BURNOTE_GCP_PROJECT
		  -o, --out-file    <file>     Set the output file (default: stdout)
		  -h, --help                   Show this help section
	EOF
}

script_name=$(basename "${BASH_SOURCE[0]}")
burn_delay="5 minutes"
prefix=burnote
secret_id=temporary_note
project=${BURNOTE_GCP_PROJECT:-nothung-de1db605}
location=europe-north1
out_file=/dev/stdout

post_note() {
	local data_file="${1:--}" expire_time
	expire_time=$(date -ud "$(date -d "+$burn_delay")" '+%Y-%m-%dT%H:%M:%SZ')

	gcloud secrets create "${default_settings[@]}" --locations="$location" --replication-policy="user-managed" --expire-time="$expire_time" --quiet \
		|| gcloud secrets update "${default_settings[@]}" --expire-time="$expire_time" --quiet

	gcloud secrets versions add "${default_settings[@]}" --data-file="$data_file"
}

read_note() {
	gcloud secrets versions access latest --secret "${default_settings[@]}" --out-file="$out_file"
}

convert_shorthand_delay() { ## [s, m, h, d] shorthand will be converted to date command compatible [seconds, minutes, hours, days]
	sed -E '
		s/^([0-9]+)\s+?([sSmMhHdD])$/\1 \l\2/
		s/ s$/ seconds/
		s/ m$/ minutes/
		s/ h$/ hours/
		s/ d$/ days/
		s/^[0-9]+$/& seconds/
		/[0-9]\s*(second|minute|hour|day)s?$/!q1
	' <<< "$1" || err "Incorrect value specified with --burn-delay flag. Run $script_name --help for more information."
}

logged_in() {
	gcloud auth list --filter=status:ACTIVE --format="value(account)" 2>&1 \
		| awk -v ec=0 '/The following filter keys were not present in any resource/{ec=1}; END{exit ec}'
}

err() {
	local bold_red='\033[1;31m' norm='\033[0m'

	printf "$bold_red%s:$norm (%s) %s\n" "ERROR" "$script_name" "$1" 1>&2
	exit 1
}

OPTIONS=s:b:p:o:h
LONGOPTS=secret-id:,burn-delay:,project:,out-file:,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$script_name" -- "$@")
eval set -- "$PARSED"

while true; do
	case "$1" in
		-s | --secret-id)
			secret_id="$2"
			shift 2
			;;
		-b | --burn-delay)
			burn_delay="$2"
			shift 2
			;;
		-p | --project)
			project="$2"
			shift 2
			;;
		-o | --out-file)
			out_file="$2"
			shift 2
			;;
		-h | --help)
			get_help
			exit 0
			;;
		--)
			shift
			break
			;;
		*) err "Internal error" ;;
	esac
done

default_settings=("$prefix-$secret_id" --project="$project")
burn_delay=$(convert_shorthand_delay "$burn_delay")

if [[ -z $1 ]]; then
	get_help

elif grep -qwF "$1_note()" "${BASH_SOURCE[0]}"; then

	if logged_in; then
		"$1_note" "$2"

	else
		err $'You need to be authorized with gcloud to run this script. Try:\n$ gcloud auth login' 1>&2
	fi

else
	err "Command $1 not found. Run $script_name --help for more information."
fi
