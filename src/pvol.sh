#!/bin/bash
# Author:      Kaffe (the@kaffemyers.com)
# Created:     2021-02-12
# Description: Volume up/down/mute/mic with pulse audio

default_sink=$(pactl info | sed '/Default Sink:/!d;s/.*\s//')
volume_step=5

set_volume() {
	local current_volume new_volume

	read -r -d '' current_volume < <(
		pactl list sinks | awk -v s="$default_sink" '$NF == s{
			while( $1 != "Volume:" ) getline
			print substr($5, 1, (length($5) - 1))
		}'
	)

	if [[ ${1} == - ]]; then
		new_volume=$((current_volume - volume_step))
		((new_volume >= 0)) || new_volume=0
	else
		new_volume=$((current_volume + volume_step))
		((new_volume <= 0)) || new_volume=100
	fi

	pactl set-sink-volume "$default_sink" $new_volume%
	echo "$new_volume"
}

case "$1" in
	up) set_volume + ;;
	down) set_volume - ;;
	mute) pactl set-sink-mute "$default_sink" toggle ;;
	mic)
		pacmd list-sources \
			| awk '/index: [0-9]/{ print $NF }' \
			| xargs -I{} pactl set-source-mute {} toggle
		;;
	*)
		echo "shitty input"
		exit 1
		;;
esac
