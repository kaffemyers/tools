.PHONY: install uninstall update help check_root

DESTDIR = /usr/local/bin
SRCDIR = src
EXECUTABLES = $(wildcard $(SRCDIR)/*)

check_root:
	@[ $$(id -u) -eq 0 ] || { echo "ERROR: Must run as root."; exit 1 ;}

install: check_root ## Install the Nothung tools
	@echo "Installing executables to $(DESTDIR)"
	@mkdir -p $(DESTDIR)
	@for file in  $(EXECUTABLES); do \
		basename=$$(basename $$file); \
		install_path=$(DESTDIR)/$${basename%.*}; \
		ln -sf $$(realpath $$file) $$install_path; \
		echo "Linked $$file to $$install_path"; \
	done
	@echo "Installation completed."

uninstall: check_root ## Uninstall the Nothung tools
	@echo "Uninstalling executables from $(DESTDIR)"
	@for file in $(EXECUTABLES); do \
		basename=$$(basename $$file); \
		install_path=$(DESTDIR)/$${basename%.*}; \
		rm -f $$install_path; \
		echo "Removed link $$install_path"; \
	done
	@echo "Uninstallation completed."

update: uninstall ## Uninstall everything, git pull to get the latest changes, install everything
	@echo "Updating the repository with git pull..."
	@git pull
	@$(MAKE) install
	@echo "Update completed."

help: ## Display available targets and their help descriptions
	@awk -F':.*?## ' -v cyan='\033[36m' -v norm='\033[0m' \
		'BEGIN { printf "Usage:\n  make " cyan "<target>" norm "\n\nTargets:\n"} \
		/^[a-zA-Z_-]+:.*?##/ { printf "  " cyan "%-20s" norm " %s\n", $$1, $$2}' $(MAKEFILE_LIST)
